def add(a, b):
    return a + b

def sub(a, b):
    return a - b


class User:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def __repr__(self):
        return self.first_name

if __name__ == "__main__":
    print("Hello World!")
